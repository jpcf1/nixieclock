/* Utility functions for the PIC16F1825 setup and configuration */

#include "../include/pic16f1825_configs.h"

void pic16f1825_init(void);
void pic16f1825_TMR2_init(void);
