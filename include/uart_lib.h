/* Utility functions for the PIC16F1825 UART module setup and configuration */

#include "../include/pic16f1825_configs.h"

void  UART_init();
void  UART_transmit(char* buffer, uint8_t buff_len);
char* UART_dec2string(int decNum);
char* UART_hour2string(uint8_t hour, uint8_t min, uint8_t sec);