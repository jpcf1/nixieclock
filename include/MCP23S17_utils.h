/* A library that interfaces the MCP23S17. Makes use of the soft_spi.h library */

#include "../include/pic16f1825_configs.h"
#include "../include/soft_spi.h"

#define IODIRA 0x00
#define IODIRB 0x01
#define IOCON  0x0A
#define OLATA  0x14
#define OLATB  0x15

void MCP23S17_init(uint8_t* buffer, uint8_t opmode, uint8_t hardware_addr);
void MCP23S17_write_portA(uint8_t* buffer, uint8_t value, uint8_t hardware_addr);
void MCP23S17_write_portB(uint8_t* buffer, uint8_t value, uint8_t hardware_addr);
void MCP23S08_init(uint8_t* buffer, uint8_t opmode, uint8_t hardware_addr);
void MCP23S08_write(uint8_t* bufferA, uint8_t value, uint8_t hardware_addr);