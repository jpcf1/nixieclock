/* The software SPI library */

#include "../include/pic16f1825_configs.h"

void spi_send_1byte(unsigned char* buf);
void spi_send_Nbytes(unsigned char* buf, int numBytes);