/* Utility functions for the PIC16F1825 setup and configuration */

#include "../include/uart_lib.h"
#include "../include/pic16f1825_configs.h"

void UART_init() {
    
    // Configure the baudrate for 9600 at Fosc 16MHz
    SPBRGL = 25;
    SPBRGH = 0x00;
    BAUDCONbits.BRG16 = 0;
    
    // Configures RC4 bit as TX bit
    RCSTAbits.SPEN = 1;
    
    // Configures the port for asynchronous transmission
    TXSTAbits.SYNC = 0;
    
    // Enables the transmission block
    TXSTAbits.TXEN = 1;
    
}

void UART_transmit(char* buffer, uint8_t buff_len) {
    for(int i=0; i < buff_len; i++) {
        TXREG = buffer[i];
        while(TXSTAbits.TRMT != 1);
    }
}

char* UART_dec2string(int decNum) {
    
    char str[3];
    
    // This works for small numbers, will eventually change this to any size
    if(decNum < 10) {
        str[2] = 0x30 + (decNum);
        str[1] = 0x30;
    }
    else if(decNum >= 10) {
        str[2] = 0x30 + (decNum%10);
        str[1] = 0x30 + (decNum/10);
    }
    
    str[0] = '\n';
    
    return str;
}

char* UART_hour2string(uint8_t hour, uint8_t min, uint8_t sec) {
    char str[9];
    
    str[1] = 0x30 + (hour%10);
    str[0] = 0x30 + (hour/10);
    
    str[2] = ':';
    
    str[4] = 0x30 + (min%10);
    str[3] = 0x30 + (min/10);

    
    str[5] = ':';
    
    str[7] = 0x30 + (sec%10);
    str[6] = 0x30 + (sec/10);
    
    str[8] = '\n';
    
    return str;
}