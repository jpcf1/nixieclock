/* Utility functions for the PIC16F1825 setup and configuration */

#include "../include/pic16f1825_utils.h"

void pic16f1825_init(void){
    /* 16MHz clock */
    OSCCONbits.SCS = 0; /* clear SCS bits <1:0> */ 
    OSCCONbits.IRCF = 0b1111; /* Set IRCF<3:0> to 1111 ( bits 3-6 of OSCCON) */
    OSCTUNE = 0x06; /* Tunes the PLL for better accuracy */
    while(OSCSTATbits.HFIOFR == 0 || OSCSTATbits.HFIOFS == 0); /* wait for clock to get stable */
}

void pic16f1825_TMR2_init(void) {
    
    // Configures and activates interrupts
    INTCONbits.GIE  = 1; // Globally activates interrupts
    INTCONbits.PEIE = 1; // Enables peripheral interrupts, where TMR2 is located
    PIE1bits.TMR2IE = 1; // Enables interrupts for TMR2
    
    // Configures timer 2
    TMR2  = 0x00;
    PR2   = 249; // With 1MHz clock and 1:64 & 1:16, this gives an interrupt each second
    T2CON = 0x7C; // Prescaler to 1 and Postscaler to 16
    T2CONbits.TMR2ON = 1; // Turns on the Timer
}

void pic16f1825_TMR1_init(void) {
    
    // Configures and activates interrupts
    INTCONbits.GIE  = 1; // Globally activates interrupts
    INTCONbits.PEIE = 1; // Enables peripheral interrupts, where TMR2 is located
    PIE1bits.TMR1IE = 1; // Enables interrupts for TMR1
    
    // Configures timer 1
    T1CONbits.T1CKPS  = 0x0;  // Prescaler value to 1
    T1CONbits.TMR1CS  = 0b10; // Clock source is set to external oscilator
    T1CONbits.T1OSCEN = 1;    // Enables the external oscillator
    TMR1H = 0x80;
    TMR1L = 0x00;
    T1CONbits.TMR1ON = 1;
    
    /*
    // Configures timer 1
    T1CONbits.T1CKPS = 0x0; // Prescaler value to 1
    T1CONbits.TMR1CS = 0x0; // Clock source is set to Fosc/4
    TMR1H = 0x63;
    TMR1L = 0xC0;
    T1CONbits.TMR1ON = 1;
     */
}
