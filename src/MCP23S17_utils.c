/* A library that interfaces the MCP23S17. Makes use of the soft_spi.h library */

#include "../include/MCP23S17_utils.h"

void MCP23S17_init(uint8_t* buffer, uint8_t opmode, uint8_t hardware_addr) {
    
    /* OPERATION MODES (PortA, PortB)
     * 00 -> (in, in)
     * 01 -> (in, out)
     * 10 -> (out, in)
     * 11 -> (out, out)
     */
    
    
    buffer[0] = 0b01000000; //The first time we write the address must be 000, since by default the HW pins are disabled
    buffer[1] = IOCON;
    buffer[2] = 0b00001000;
   
    spi_send_Nbytes(buffer, 3);
    
    
    buffer[0] = (0b0100 << 4) | (hardware_addr << 1) | 0b0;
    buffer[1] = IODIRA;
    buffer[2] = 0x00;
   
    spi_send_Nbytes(buffer, 3);
    
    buffer[0] = (0b0100 << 4) | (hardware_addr << 1) | 0b0;
    buffer[1] = IODIRB;
    buffer[2] = 0x00;
   
    spi_send_Nbytes(buffer, 3);
}

void MCP23S17_write_portA(uint8_t* bufferA, uint8_t value, uint8_t hardware_addr) {
    
    uint8_t buffer[3];
    
    buffer[0] = (0b0100 << 4) | (hardware_addr << 1) | 0b0;
    buffer[1] = OLATA;
    buffer[2] = value;
   
    spi_send_Nbytes(buffer, 3);
}

void MCP23S07_write_portB(uint8_t* bufferB, uint8_t value, uint8_t hardware_addr) {
    
    uint8_t buffer[3];
    
    buffer[0] = (0b0100 << 4) | (hardware_addr << 1) | 0b0;
    buffer[1] = OLATB;
    buffer[2] = value;
   
    spi_send_Nbytes(buffer, 3);
}

void MCP23S08_init(uint8_t* buffer, uint8_t opmode, uint8_t hardware_addr) {
    
    /* OPERATION MODES (PortA, PortB)
     * 00 -> (in, in)
     * 01 -> (in, out)
     * 10 -> (out, in)
     * 11 -> (out, out)
     */
    
    
    buffer[0] = 0b01000000; //The first time we write the address must be 000, since by default the HW pins are disabled
    buffer[1] = 0x05;  // IOCON
    buffer[2] = 0b00000000;
   
    spi_send_Nbytes(buffer, 3);
    
    
    buffer[0] = (0b0100 << 4) | (hardware_addr << 1) | 0b0;
    buffer[1] = 0x00; // IODIRA
    buffer[2] = 0x00;
   
    spi_send_Nbytes(buffer, 3);
}

void MCP23S08_write(uint8_t* bufferA, uint8_t value, uint8_t hardware_addr) {
    
    uint8_t buffer[3];
    
    buffer[0] = (0b0100 << 4) | (hardware_addr << 1) | 0b0;
    buffer[1] = 0x0A; // OLATA;
    buffer[2] = value;
   
    spi_send_Nbytes(buffer, 3);
}