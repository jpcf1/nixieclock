/* The software SPI library */

#include "../include/soft_spi.h"

void spi_send_1byte(unsigned char* buf) {
    
    SPI_SCK = 0;
    __delay_us(1);
    
    SPI_SS = 0;
    __delay_us(1);
    
   for(int i=0; i < 8; i++) {
        SPI_SCK = 0; /* Clock goes idle */

        __delay_us(2);

        /* Since we send the MSB first, we mask bit 8 and we send its logical value */
        (*buf) & 0b10000000 ? SPI_SDO = 1 : SPI_SDO = 0;  

        /* Rising Edge of the Clock */
        __delay_us(2);
        SPI_SCK = 1;
        __delay_us(2);

        /* We shift the next bit */
       (*buf) = ((*buf)<<1);
    }

    SPI_SCK = 0;  
    SPI_SS  = 1;
    
}

void spi_send_Nbytes(unsigned char* buf, int numBytes) {
    
    unsigned char tempByte = 0;
    
    SPI_SCK = 0;
    __delay_us(1);
    
    SPI_SS = 0;
    __delay_us(1);
    
    for(int n=0; n < numBytes; n++) {
        tempByte = *(buf+n);
        for(int i=0; i < 8; i++) {
            SPI_SCK = 0; /* Clock goes idle */
            
            __delay_us(1);
            
            /* Since we send the MSB first, we mask bit 8 and we send its logical value */
            tempByte & 0b10000000 ? SPI_SDO = 1 : SPI_SDO = 0;  

            /* Rising Edge of the Clock */
            __delay_us(3);
            SPI_SCK = 1;
            __delay_us(2);

            /* We shift the next bit */
            tempByte = (tempByte<<1);
        }
    }
    SPI_SCK = 0;  
    SPI_SS  = 1;
}

