#include <PIC16F1825.h>
#include <xc.h>
#include <stdint.h>

#include "../include/pic16f1825_configs.h"
#include "../include/pic16f1825_utils.h"
#include "../include/soft_spi.h"
#include "../include/MCP23S17_utils.h"
#include "../include/uart_lib.h"

#define ENABLED  1
#define DISABLED 0
#define TMR1DELAY 0x63C0


#define HW_ADDR_IOEX1 0b00000011
#define HW_ADDR_IOEX2 0b00000010

// The time struct definition
typedef struct time_t {
    uint8_t hour;
    uint8_t min;
    uint8_t sec;
    uint16_t msec;
} time_t;


// The time struct that keeps the current time
time_t curr_time = {21, 18, 0, 0};
int count = 0;

void advance_time(void) {
    
    // Advances a second and updates the other vars
    if(curr_time.sec == 59) {

        curr_time.sec = 0;

        if(curr_time.min == 59) {

            curr_time.min = 0;

            if(curr_time.hour == 23) {

                curr_time.min = 0;
                curr_time.sec = 0;
                curr_time.hour = 0;

            }
            else {
                curr_time.hour++;
            }
        }
        else {
            curr_time.min++;
        }
    }
    else {
        curr_time.sec++;
    }

}

void interrupt isr() {
    // Checks the interrupt vector to assert which was the source
    if(PIR1bits.TMR1IF) {
        TMR1H = 0x80;
        TMR1L = 0x00;
        PIR1bits.TMR1IF = 0;
            
        // Toggles led for delay measurement feedback
        LATC3 = ~LATC3;
        advance_time();
        UART_transmit(UART_hour2string(curr_time.hour, curr_time.min, curr_time.sec), 9);

    }
}

void main(void) {
    
    // A temporary buffer for transmission
    uint8_t buffer[3] = "ola";
    
    // Sets clock to internal generator @ 16MHz
    pic16f1825_init();
    
    // Configure IO pin direction (as outputs)
    TRISC = 0x00;
    TRISA = 0x00;
    ANSELC = 0x00;
    ANSELA = 0x00;
    LATC3 = 1;
    
    // Configures the UART Port speed and settings
    UART_init();
    
    // Configures the Timer1 Operation
    pic16f1825_TMR1_init();
    
    while(1) {      
    }
    
    return;
}

/*
inline uint8_t dec_to_7seg(uint8_t decimal) {
    switch(decimal) {
        case 0:
            return 0x77;
        case 1:
            return 0x24;
        case 2:
            return 0x5D;
        case 3:
            return 0x6D;
        case 4:
           return 0x2E;
        case 5:
            return 0x6B;
        case 6:
            return 0x7B;
        case 7:
            return 0x25;
        case 8:
            return 0x7F;
        case 9:
            return 0x2F;
    }
}
*/

/*
 void main(void) {
    
    // A temporary buffer for transmission
    uint8_t buffer[3];
    
    // The time struct that keeps the current time
    //time_t curr_time = {0, 0, 0};
    
    // Sets clock to internal generator @ 16MHz
    pic16f1825_init();
    
    // Configure IO pin direction (as outputs)
    TRISC = 0x00;
    TRISA = 0x00;
    ANSELC = 0x00;
    ANSELA = 0x00;

    // Configure SPI operation
    SPI_SCK = 0;
    SPI_SDI = 0; // this prevents reading garbage
    SPI_SDO = 0;
    SPI_SS  = 1;
    WR = 1;
    
    MCP23S08_init(buffer, 0, HW_ADDR_IOEX1); //MCP23S17_init(buffer, 0, HW_ADDR_IOEX1);
    
    
    // Configures the UART Port speed and settings
    //UART_init();
    
    // Configures interrupts
    pic16f1825_TMR2_init();
    
    __delay_ms(1);

    
    while(1) {  
        
        // Waits for the completion of one millisecond
        while(PIR1bits.TMR2IF != 1);
        PIR1bits.TMR2IF = 0;
        
        if(curr_time.msec == 999) {
            
            curr_time.msec = 0;
            
            if(curr_time.sec == 59) {

                curr_time.sec = 0;

                if(curr_time.min == 59) {

                    curr_time.min = 0;

                    if(curr_time.hour == 23) {
                        curr_time.hour = 0;
                    }
                    else {
                        curr_time.hour++;
                    }
                }
                else {
                    curr_time.min++;
                }
            }
            else {
                curr_time.sec++;
            }
            write_display(curr_time.hour, curr_time.min, curr_time.sec);
            //UART_transmit(UART_hour2string(curr_time.hour, curr_time.min, curr_time.sec), 9);
        }
        else
            curr_time.msec++;
        
    }
    
    return;
}
 */

void write_digit_SLR2016(uint8_t digit, uint8_t ascii_value){

    uint8_t buffer[3];
    
    A0 = (digit%4);
    A1 = (digit%4)>>1;
    MCP23S08_write(buffer, ascii_value, HW_ADDR_IOEX1); //MCP23S17_write_portB(buffer, ascii_value, HW_ADDR_IOEX1);

    if(digit/4 == 1) {
        __delay_us(1);
        WR_L = 0;
        __delay_us(1);
        WR_L = 1;
    }
    else {
        __delay_us(1);
        WR_R = 0;
        __delay_us(1);
        WR_R = 1;
    }
}

void write_display(uint8_t hour, uint8_t minute, uint8_t second){
    write_digit_SLR2016(7, hour/10 + 0x30);
    write_digit_SLR2016(6, hour%10 + 0x30);
    write_digit_SLR2016(5, 0x3A);
    write_digit_SLR2016(4, minute/10 + 0x30);
    write_digit_SLR2016(3, minute%10 + 0x30);
    write_digit_SLR2016(2, 0x3A);
    write_digit_SLR2016(1, second/10 + 0x30);
    write_digit_SLR2016(0, second%10 + 0x30);
    
}
